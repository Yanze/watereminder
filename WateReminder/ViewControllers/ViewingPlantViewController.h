//
//  ViewingPlantViewController.h
//  WateReminder
//
//  Created by yanze on 8/26/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "HCSStarRatingView.h"
#import "Plant.h"

@interface ViewingPlantViewController : UIViewController <UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSManagedObject *plant;
@property (nonatomic, assign) BOOL isNewPlant;
@property (weak, nonatomic) IBOutlet UIPickerView *wateringPicker;

@end
