//
//  PlantDetailViewController.h
//  WateReminder
//
//  Created by yanze on 8/7/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class Plant;
@interface PlantDetailViewController : UIViewController <UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSManagedObject *plant;
@property (nonatomic, assign) BOOL isNewPlant;
@property (weak, nonatomic) IBOutlet UIPickerView *waterFrequencyPicker;

@end
