//
//  ViewingPlantViewController.m
//  WateReminder
//
//  Created by yanze on 8/26/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import "ViewingPlantViewController.h"
#import "PlantDetailViewController.h"
#import "PlantListViewController.h"

@import ExpandingMenu;

@interface ViewingPlantViewController ()


@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *minTemperature;
@property (weak, nonatomic) IBOutlet UILabel *maxTemperature;

@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property (nonatomic, strong) NSNumber *lightIntensity;

@property (nonatomic, strong) NSArray *pickerData;
@property (nonatomic, assign) NSInteger watering;

@property (nonatomic, strong) NSMutableArray *plantList;
@property (nonatomic, assign)NSInteger index;

@end

@implementation ViewingPlantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self lightsRating];
    self.pickerData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15"];
    self.wateringPicker.dataSource = self;
    self.wateringPicker.delegate = self;
    [self.wateringPicker setUserInteractionEnabled:NO];
    [self.wateringPicker setAlpha:1];
    [self setSwipe];
    [self setupMenu];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadPlant];
}

- (void)setSwipe {
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle)];
    swipeRight.numberOfTouchesRequired = 1;
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
}

- (void)rightSwipeHandle {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) setDeaultImage {
    UIImage *image = [UIImage imageNamed: @"placeholder.png"];
    [self.imageView setImage:image];
}


- (void)loadPlant {
    if(self.plant) {
        
        NSNumber *temperatureMin = (NSNumber *)[self.plant valueForKey:@"temperatureMin"];
        [self.minTemperature setText:temperatureMin.stringValue];
        NSNumber *temperatureMax = (NSNumber *)[self.plant valueForKey:@"temperatureMax"];
        [self.maxTemperature setText:temperatureMax.stringValue];
        
        NSNumber* lightsIntensity = (NSNumber*)[self.plant valueForKey:@"lightsIntensity"];
        self.starRatingView.value = [lightsIntensity floatValue];
        
        NSNumber *watering = (NSNumber *)[self.plant valueForKey:@"waterFrequency"];
        [self.wateringPicker selectRow:[watering intValue]-1 inComponent:0 animated:YES];
        
        UIImage *image = [UIImage imageWithData:[self.plant valueForKey:@"image"]];
        [self.imageView setImage:image];
        
        self.navigationItem.title = [self.plant valueForKey:@"name"];
        
    }
    
}

- (void)lightsRating {
    self.starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(90, 450, 200, 50)];
    self.starRatingView.maximumValue = 5;
    self.starRatingView.minimumValue = 0;
    self.starRatingView.allowsHalfStars = YES;
    self.starRatingView.enabled = NO;
    [self.starRatingView setAlpha:1];
    self.starRatingView.tintColor = [UIColor orangeColor];
    self.starRatingView.emptyStarImage = [[UIImage imageNamed:@"emptySun"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.starRatingView.filledStarImage = [[UIImage imageNamed:@"sun"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.starRatingView];
}

- (void)didChangeValue:(HCSStarRatingView *)sender {
    self.lightIntensity = @(sender.value);
}

// grab managedObjectContext
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.pickerData count];
}


// return the data displayed in each row in Picker
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    self.watering = [self.pickerData[row] intValue];
    return self.pickerData[row];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EditingPlant"]) {
        PlantDetailViewController *pdvc = segue.destinationViewController;
        pdvc.plant = self.plant;
        pdvc.isNewPlant = YES;
    }
}

-(void)setupMenu{
    CGRect buttonFrame = CGRectMake(300.0, 600.0, 64.0, 64.0);
    ExpandingMenuButton* menuButton = [[ExpandingMenuButton alloc] initWithFrame:buttonFrame
                                                                     centerImage:[UIImage imageNamed:@"chooser-button-tab"]
                                                          centerHighlightedImage:[UIImage imageNamed:@"chooser-button-tab-highlighted"]];
    menuButton.bottomViewAlpha = 0.6;
    [self.view addSubview:menuButton];
    
    CGSize buttonSize = CGSizeMake(64.0, 64.0);
    
    ExpandingMenuItem* editPlant = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                  image:[UIImage imageNamed:@"chooser-moment-icon-thought"]
                                                       highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-thought-highlighted"]
                                                        backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                             backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                             itemTapped: ^{
                                                                 [self performSegueWithIdentifier:@"EditingPlant" sender:self];
                                                             }];
    
    ExpandingMenuItem* myPlants = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                       image:[UIImage imageNamed:@"chooser-moment-icon-camera"]
                                                            highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-camera-highlighted"]
                                                             backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                                  backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                                  itemTapped:^{
                                                                      NSLog(@"modal");
                                                                      [self setModalPresentationStyle:UIModalPresentationCurrentContext];
                                                                      [self performSegueWithIdentifier:@"howToUseApp" sender:self];
                                                                  }];
    
    ExpandingMenuItem* plantSuggestion = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                           image:[UIImage imageNamed:@"chooser-moment-icon-place"]
                                                                highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-place-highlighted"]
                                                                 backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                                      backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                                      itemTapped:^{
                                                                          [self performSegueWithIdentifier:@"plantSuggestion" sender:self];
                                                                      }];
    
    editPlant.title = @"Edit plant";
    myPlants.title = @"Back to Plant List";
    plantSuggestion.title = @"Find a new plant";
    
    editPlant.titleColor = [UIColor whiteColor];
    myPlants.titleColor = [UIColor whiteColor];
    plantSuggestion.titleColor = [UIColor whiteColor];
    
    [menuButton addMenuItems:@[plantSuggestion, myPlants, editPlant]];
    
}




@end
