//
//  ViewController.h
//  WateReminder
//
//  Created by yanze on 8/7/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSKAirbnbStretchyHeaderView.h"


@interface PlantListViewController : UIViewController <GSKAirbnbStretchyHeaderViewDelegate, UITableViewDataSource, UIScrollViewDelegate>


@end

