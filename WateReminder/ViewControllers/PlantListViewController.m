//
//  ViewController.m
//  WateReminder
//
//  Created by yanze on 8/7/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import "PlantListViewController.h"
#import "PlantDetailViewController.h"
#import "ViewingPlantViewController.h"
#import "Plant.h"
#import <CoreData/CoreData.h>
#import "Utils.h"
#import "CustomUITableViewCell.h"

@import ExpandingMenu;
@import GSKStretchyHeaderView;


@interface PlantListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *plantList;
@property (nonatomic, assign)NSInteger index;
@property (nonatomic, strong)GSKAirbnbStretchyHeaderView *stretchyHeader;


@end

@implementation PlantListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setHeader];
    [self.navigationController setNavigationBarHidden:YES];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Plant"];
    self.plantList = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];
    
    [self setupMenu];
}

- (void)setHeader {
    CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, 250);
    GSKAirbnbStretchyHeaderView *headerView = [[GSKAirbnbStretchyHeaderView alloc] initWithFrame:frame];
    headerView.maximumContentHeight = 300;
    headerView.minimumContentHeight = 84;
    headerView.contentBounces = YES;
    headerView.delegate = self;
    [self.tableView addSubview:headerView];
}


- (void)airbnbStretchyHeaderView:(GSKAirbnbStretchyHeaderView *)headerView
              didTapSearchButton:(id)sender {
    CGPoint contentOffset = self.tableView.contentOffset;
    if (contentOffset.y < -self.stretchyHeader.minimumContentHeight) {
        contentOffset.y = -self.stretchyHeader.minimumContentHeight;
        [self.tableView setContentOffset:contentOffset animated:YES];
    }
}
- (void)airbnbStretchyHeaderView:(GSKAirbnbStretchyHeaderView *)headerView
                 didTapSearchBar:(id)sender {
    
}



-(void)setupMenu{
    CGRect buttonFrame = CGRectMake(300.0, 600.0, 64.0, 64.0);
    ExpandingMenuButton* menuButton = [[ExpandingMenuButton alloc] initWithFrame:buttonFrame
                                                                     centerImage:[UIImage imageNamed:@"chooser-button-tab"]
                                                          centerHighlightedImage:[UIImage imageNamed:@"chooser-button-tab-highlighted"]];
    menuButton.bottomViewAlpha = 0.6;
    [self.view addSubview:menuButton];
    
    CGSize buttonSize = CGSizeMake(64.0, 64.0);
    
    ExpandingMenuItem* addNew = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                           image:[UIImage imageNamed:@"chooser-moment-icon-thought"]
                                                                highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-thought-highlighted"]
                                                                 backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                                      backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                             itemTapped: ^{
                                                                 [self performSegueWithIdentifier:@"addNew" sender:self];
                                                             }];
    
    ExpandingMenuItem* howToUseApp = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                 image:[UIImage imageNamed:@"chooser-moment-icon-camera"]
                                                      highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-camera-highlighted"]
                                                       backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                            backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                                  itemTapped:^{
                                                                      NSLog(@"modal");
                                                                      [self setModalPresentationStyle:UIModalPresentationCurrentContext];
                                                                      [self performSegueWithIdentifier:@"howToUseApp" sender:self];
                                                                  }];
    
    ExpandingMenuItem* plantSuggestion = [[ExpandingMenuItem alloc] initWithSize:buttonSize
                                                                           image:[UIImage imageNamed:@"chooser-moment-icon-place"]
                                                                highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-place-highlighted"]
                                                                 backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                                      backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]
                                                                      itemTapped:^{
                                                                          [self performSegueWithIdentifier:@"plantSuggestion" sender:self];
                                                                      }];
    
    addNew.title = @"Add new plant";
    howToUseApp.title = @"How to use the app";
    plantSuggestion.title = @"Find a new plant";
    
    addNew.titleColor = [UIColor whiteColor];
    howToUseApp.titleColor = [UIColor whiteColor];
    plantSuggestion.titleColor = [UIColor whiteColor];
    
    [menuButton addMenuItems:@[plantSuggestion, howToUseApp, addNew]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.plantList count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleIdentifier = @"Cell";
    CustomUITableViewCell *cell = (CustomUITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleIdentifier forIndexPath:indexPath];
    
    // configure the cell
    NSManagedObject *plant = [self.plantList objectAtIndex: indexPath.row];
    
    cell.plantName.text = [plant valueForKey:@"name"];
    cell.watering.text = [NSString stringWithFormat:@"Watering every %@ days", [plant valueForKey: @"waterFrequency"]];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageWithData:[plant valueForKey:@"image"]] stretchableImageWithLeftCapWidth:5.0 topCapHeight:5.0]];
    cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[[UIImage imageWithData:[plant valueForKey:@"image"]] stretchableImageWithLeftCapWidth:5.0 topCapHeight:5.0] ];
    cell.selectedBackgroundView.alpha = 0.5;    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.index = indexPath.row;
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Viewing"]) {
        NSManagedObject *selectedPlant = [self.plantList objectAtIndex:[[self.tableView indexPathForSelectedRow]row]];
        
        ViewingPlantViewController *vpvc = segue.destinationViewController;
        vpvc.plant = selectedPlant;
        vpvc.isNewPlant = NO;
    }
//    else if ([segue.identifier isEqualToString:@"addNew"]) {
//        PlantDetailViewController *pdvc = segue.destinationViewController;
//        pdvc.plant = [[Plant alloc]init];
//        pdvc.isNewPlant = YES;
//    }
    
}

// delete item
- (void)tableView: (UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSManagedObjectContext *context = [self managedObjectContext];
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [context deleteObject:[self.plantList objectAtIndex:indexPath.row]];
        // invoke the save method to commit the change
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        // remove the deleted plant from table view
        [self.plantList removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}




@end
