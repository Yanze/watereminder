//
//  PlantDetailViewController.m
//  WateReminder
//
//  Created by yanze on 8/7/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import "PlantDetailViewController.h"
#import "PlantListViewController.h"
#import "HCSStarRatingView.h"
#import "Plant.h"


@interface PlantDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *plantNameField;
@property (weak, nonatomic) IBOutlet UITextField *minTemperature;
@property (weak, nonatomic) IBOutlet UITextField *maxTemperature;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (nonatomic, retain) NSData *imgData;

@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property (nonatomic, strong) NSNumber *lightIntensity;
@property (nonatomic, strong) NSArray *pickerData;
@property (nonatomic, assign) NSInteger watering;


@end

@implementation PlantDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self lightsRating];
    self.waterFrequencyPicker.dataSource = self;
    self.waterFrequencyPicker.delegate = self;
    [self setWaterPickData];
    [self setDeaultImage];
    [self setGestureRecognizer];
    [self loadPlant];
    
}

- (void)setGestureRecognizer {
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(takePicture:)];
    [self.imageView addGestureRecognizer:singleFingerTap];
    [self.imageView setUserInteractionEnabled:YES];
}

- (void)setWaterPickData {
    self.pickerData = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15"];
}


- (void) setDeaultImage {
    UIImage *image = [UIImage imageNamed: @"placeholder.png"];
    [self.imageView setImage:image];
}

- (void)loadPlant {
    if(self.plant) {

        [self.plantNameField setText:[self.plant valueForKey:@"name"]];
        
        NSNumber *temperatureMin = (NSNumber *)[self.plant valueForKey:@"temperatureMin"];
        [self.minTemperature setText:temperatureMin.stringValue];
        NSNumber *temperatureMax = (NSNumber *)[self.plant valueForKey:@"temperatureMax"];
        [self.maxTemperature setText:temperatureMax.stringValue];
        
        NSNumber* lightsIntensity = (NSNumber*)[self.plant valueForKey:@"lightsIntensity"];
        self.starRatingView.value = [lightsIntensity floatValue];
        
        NSNumber *watering = (NSNumber *)[self.plant valueForKey:@"waterFrequency"];
        [self.waterFrequencyPicker selectRow:[watering intValue]-1 inComponent:0 animated:YES];

        UIImage *image = [UIImage imageWithData:[self.plant valueForKey:@"image"]];
        [self.imageView setImage:image];

    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *currentPlant = self.plant ? self.plant : [NSEntityDescription insertNewObjectForEntityForName:@"Plant" inManagedObjectContext:context];
    
    [currentPlant setValue:self.plantNameField.text forKey:@"name"];
    [currentPlant setValue:@(self.starRatingView.value) forKey:@"lightsIntensity"];
    [currentPlant setValue:@(self.watering) forKey:@"waterFrequency"];
    
    NSNumberFormatter *n = [[NSNumberFormatter alloc]init];
    n.numberStyle = NSNumberFormatterDecimalStyle;
    [currentPlant setValue:[n numberFromString:self.minTemperature.text] forKey:@"temperatureMin"];
    [currentPlant setValue:[n numberFromString:self.maxTemperature.text] forKey:@"temperatureMax"];
    
    self.imgData = UIImageJPEGRepresentation(self.imageView.image, 0.9);
    [currentPlant setValue:self.imgData forKey:@"image"];
    
    NSError *error = nil;
    if(![context save: &error]) {
        NSLog(@"Can't save %@ %@", error, [error localizedDescription]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.pickerData count];
}


// return the data displayed in each row in Picker
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    self.watering = [self.pickerData[row] intValue];
    return self.pickerData[row];
}


- (void)lightsRating {
    self.starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(90, 390, 200, 50)];
    self.starRatingView.maximumValue = 5;
    self.starRatingView.minimumValue = 0;
    self.starRatingView.allowsHalfStars = YES;
    self.starRatingView.tintColor = [UIColor orangeColor];
    self.starRatingView.emptyStarImage = [[UIImage imageNamed:@"emptySun"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.starRatingView.filledStarImage = [[UIImage imageNamed:@"sun"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.starRatingView];
}

- (void)didChangeValue:(HCSStarRatingView *)sender {
    self.lightIntensity = @(sender.value);
}


- (void)takePicture:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePicker.delegate = self;
    // place image picker on the screen
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    // get picked image from info dictionary
//    UIImage *image = info[UIImagePickerControllerOriginalImage];
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.imageView.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}


// grab managedObjectContext
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// tap background to dismiss the keyboard
- (IBAction)backgroundTapped:(id)sender {
    [self.view endEditing:YES];
}




@end
