//  Plant.h
//  WateReminder
//
//  Created by yanze on 8/7/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface Plant : NSManagedObject

@end

#import "Plant+CoreDataProperties.h"
