//
//  Plant+CoreDataProperties.h
//  WateReminder
//
//  Created by yanze on 8/23/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Plant.h"

NS_ASSUME_NONNULL_BEGIN

@interface Plant (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSNumber *isIndoor;
@property (nullable, nonatomic, retain) NSNumber *lightsIntensity;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *temperatureMax;
@property (nullable, nonatomic, retain) NSNumber *temperatureMin;
@property (nullable, nonatomic, retain) NSNumber *waterFrequency;

@end

NS_ASSUME_NONNULL_END
