//
//  CustomUITableViewCell.h
//  WateReminder
//
//  Created by yanze on 9/2/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUITableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *plantName;
@property(nonatomic, weak) IBOutlet UILabel *watering;

@end
