//
//  PlantList.m
//  WateReminder
//
//  Created by yanze on 8/10/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import "PlantList.h"
#import "Plant.h"

@interface PlantList ()
@property (nonatomic, strong) NSMutableArray<Plant*> *allPlants;
@end


@implementation PlantList

+ (instancetype) sharedInstance {
    static PlantList *plantList = nil;
    if(!plantList) {
        plantList = [[self alloc]initPrivate];
    }
    return plantList;
}

- (instancetype)initPrivate {
    self = [super init];
    if(self) {
        self.allPlants = [[NSMutableArray alloc]init];
    }
    return self;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"shared instance" reason:@"..." userInfo:nil];
}

- (NSArray<Plant*> *) getAllPlants {
//    NSMutableArray *copy = [[NSMutableArray alloc]initWithArray:self.allPlants copyItems: YES];
    return self.allPlants;
}

- (void) addPlant: (Plant *)plant {
    [self.allPlants addObject:plant];
}

- (void) removePlant: (Plant *)plant {
    [self.allPlants removeObject:plant];
}

@end
