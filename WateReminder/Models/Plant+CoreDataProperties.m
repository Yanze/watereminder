//
//  Plant+CoreDataProperties.m
//  WateReminder
//
//  Created by yanze on 8/23/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Plant+CoreDataProperties.h"

@implementation Plant (CoreDataProperties)

@dynamic createdAt;
@dynamic image;
@dynamic isIndoor;
@dynamic lightsIntensity;
@dynamic name;
@dynamic temperatureMax;
@dynamic temperatureMin;
@dynamic waterFrequency;

@end
