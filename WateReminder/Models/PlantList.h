//
//  PlantList.h
//  WateReminder
//
//  Created by yanze on 8/10/16.
//  Copyright © 2016 yanzeliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Plant;
@interface PlantList : NSObject

+ (instancetype) sharedInstance;

- (NSArray<Plant*> *) getAllPlants;
- (void) addPlant: (Plant *)plant;
- (void) removePlant: (Plant *)plant;

@end
